let mySound, amplitude;

function preload() {
  mySound = loadSound('music.mp3');
}

function setup() {
  createCanvas(displayWidth, displayHeight);
  console.log("setup")
  amplitude = new p5.Amplitude();
}

// Continuously called in p5
function draw() {
  let level = amplitude.getLevel();
  // Map level value to a visible one
  let diameter = map(level,0,1,10,400);
  background(1);
  fill(color(255,0,0));
  stroke(255);
  strokeWeight(4);
  ellipse(displayWidth/2,displayHeight/2,diameter,diameter);
}

function mousePressed() {
  if (mySound.isPlaying()) {
    mySound.stop();
  } else {
    mySound.play();
  }